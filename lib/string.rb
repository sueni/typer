%w{ spaces multimarks dashes nonbreaking_space }.each do |m|
  require_relative m
end

class String
  def typo
    NonbreakingSpace.handle do
      Dashes.handle do
        Spaces.handle do
          Multimarks.handle do
            self
          end
        end
      end
    end
  end
end
