module Dashes

  DASHES = [
    HYPHEN = "\u002d",
    ENDASH = "\u2013",
    EMDASH = "\u2014",
  ].join

  extend self

  def handle
    en_dash_between_numbers do
      hyphen_minus_after_word do
        em_dash_at_start_of_line_or_after_blank do
          yield
        end
      end
    end
  end

  private

    def em_dash_at_start_of_line_or_after_blank
      yield.gsub(/(?<=\A|\p{Blank})(?:#{HYPHEN}{1,2}|#{ENDASH})(?!\d)/, EMDASH)
    end

    def hyphen_minus_after_word
      yield.gsub(/(?<=\p{Alpha})[#{ENDASH}#{EMDASH}]/, HYPHEN)
    end

    def en_dash_between_numbers
      yield.gsub(/(?<=[\dIVX]|[\dIVX][  ])
                 [#{HYPHEN}#{EMDASH}]
                 (?=[  ]?[\dIVX])/x, ENDASH)
    end
end
