require_relative 'quote'
require_relative 'dashes'
require_relative 'nonbreaking_space'

module Spaces
  @oqs = Quote::OPENING_QUOTES.join
  @cqs = Quote::CLOSING_QUOTES.join
  NO_SPACE_BEFORE_PUNCTS = '.,?!);:'
  SPACE_AFTER_PUNCTS     = '.,?!)'

  extend self

  def handle
    add_nbs_between_some_punct_and_dashes do
      remove_inside_fractional_measure_name do
        remove_near_quotes do
          squeeze do
            keep_space_after_punct do
              yield
            end
          end
        end
      end
    end
  end

  private

    def squeeze
      yield.gsub(/(\p{Blank}){2,}/, '\1')
    end

    def keep_space_after_punct
      yield.gsub(/ +([#{NO_SPACE_BEFORE_PUNCTS}])/, '\1 ')
    end

    def remove_near_quotes
      yield.gsub(/(?<=[#@oqs]) (?=\p{Graph})/, '')
        .gsub(/(?<=\p{Graph}) (?=[#@cqs])/, '')
    end

    def remove_inside_fractional_measure_name
      yield.gsub(%r{(?<=\bкм|\bм) ?/ ?(?=ч\b|с\b)}, '/')
    end

    def add_nbs_between_some_punct_and_dashes
      yield.gsub(/(?<=[#{SPACE_AFTER_PUNCTS}])(?=[#{Dashes::DASHES}])/,
                 NonbreakingSpace::NBS)
    end
end
