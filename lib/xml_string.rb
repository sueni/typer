class XMLString
  def initialize(xml)
    @xml = xml
  end

  def to_s
    @xml
  end

  def prettyfy
    @xml = IO.popen(%w[xmllint --format --recover --encode UTF-8 -],
                       'r+',
                       :err => '/dev/null') do |pipe|
                         pipe.write @xml
                         pipe.close_write
                         pipe.read
                       end
  end

  def chunk_with_editable(*tags)
    patt = tags.join(?|)

    @xml.split(/(<(?:#{patt})(?: name="notes")?>.+?<\/(?:#{patt})>)/m).to_enum
  end

  def split_on_tags
    @xml.split(/(<[^>]+?>)/)
  end

  def shift_note_after_punctuation
    @xml = @xml.gsub(/(<a[^>]+?>\[?\d+\]?<\/a>)(\p{Punct}+)/, '\2\1')
    self
  end

  def purify_tag(*tags)
    tags.each do |tag|
      # pull out leading spaces out of tags (but not between opening-closing tag)
      @xml = @xml.gsub(/(?<!<\/#{(tags + [:a]).join(?|)}>)(<#{tag}>)( +?)/, '\2\1').
        # pull out trailing spaces out of tags (but not between opening-closing tag)
        gsub(/( +?)(<\/#{tag}>)(?!<(?:#{tags.join(?|)})>)/, '\2\1').

        # FIRST LVL
        # pull out the leading quote if there is no a closing quote inside tag
        gsub(/(<#{tag}>)«(?=[^»]+?<\/#{tag}>)/, '«\1').
        # pull out the trailing quote if there is no an opening quote inside tag
        gsub(/(<#{tag}>[^«]+?)»(<\/#{tag}>)/, '\1\2»').

        # SECOND LVL
        # pull out the leading quote if there is no a closing quote inside tag
        gsub(/(<#{tag}>)„(?=[^“]+?<\/#{tag}>)/, '„\1').
        # pull out the trailing quote if there is no an opening quote inside tag
        gsub(/(<#{tag}>[^„]+?)“(<\/#{tag}>)/, '\1\2“')
    end

    # pull out spaces between opening tags to the left
    @xml = @xml.gsub(/(<(?:#{tags.join(?|)})>)( +)(<(?:#{tags.join(?|)})>)/, '\2\1\3').
      # pull out spaces between closing tags to the right
      gsub(/(<\/(?:#{tags.join(?|)})>)( +)(<\/(?:#{tags.join(?|)})>)/, '\1\3\2').
      # remove meaningless spaces after <p> and before </p>
      gsub(/(?<=<p>) +/, '').gsub(/ +(?=<\/p>)/, '')

    self
  end
end
