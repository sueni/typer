module Multimarks
  extend self

  def handle
    two_or_more_independent_dots do
      ensure_only_two_dots_after_excl_or_que do
        remove_dots_after_excl_que do
          reorder_excl_que do
            yield
          end
        end
      end
    end
  end

  private

    def reorder_excl_que
      yield.gsub('!?', '?!')
    end

    def remove_dots_after_excl_que
      yield.gsub(/\?!\.+/, '?!')
    end

    def ensure_only_two_dots_after_excl_or_que
      yield.gsub(/([?!])\.+/, '\1..')
    end

    def two_or_more_independent_dots
      yield.gsub(/(?<![?!.])\.{2,}(?!\.)/) do |m|
        case m.size
        when 2      then '.'
        when (3..4) then '…'
        else m
        end
      end
    end
end
