require_relative 'dashes'

module NonbreakingSpace
  NBS = ? 
  MEASURES = '(?:(?>гг|г|тыс)\.|%|(?>км|кг|млн|млрд|м|трлн|га)\b)'
  SHORTS = {
    '([Ии]) т\. ?д\.' => '\1 т. д.',
    '([Ии]) т\. ?п\.' => '\1 т. п.',
    '([Вв]) т\. ?ч\.' => '\1 т. ч.',
    'а\. ?е\.'        => 'а. е.',
    'н\. ?э\.'        => 'н. э.',
    'т\. ?е\.'        => 'т. е.',
    'т\. ?н\.'        => 'т. н.',
    'т\. ?о\.'        => 'т. о.',
    'т\. ?к\.'        => 'т. к.',
  }

  extend self

  def handle
    chunk_numbers_by_triple_digits do
      between_digits do
        between_initials do
          inside_certain_short_phrases do
            between_number_paragraph_chapter_and_a_digit do
              between_digit_and_short_measure_name do
                between_roman_digit_and_century do
                  between_punct_and_emdash do
                    yield
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  private

    def between_punct_and_emdash
      yield.gsub(/(?<=[.,!?)]) (?=#{Dashes::EMDASH})/, NBS)
    end

    def between_roman_digit_and_century
      yield.gsub(/(?<=[IVX]) (?=в\.)/, NBS)
    end

    def between_digit_and_short_measure_name
      yield.gsub(/(?<=\d) (?=#{MEASURES})/, NBS)
    end

    def between_number_paragraph_chapter_and_a_digit
      yield.gsub(/(?<=[№§]|[Гг]л\.) ?(?=\d)/, NBS)
    end

    def inside_certain_short_phrases
      str = yield
      SHORTS.each do |k,v|
        str = str.gsub(Regexp.new('\b' + k), v)
      end
      str
    end

    def between_initials
      yield.gsub(/(?<=\b[А-Я]\.) ?(?=[А-Я])/, NBS)
    end

    def between_digits
      yield.gsub(/(?<=\d) (?=\d{3})/, NBS)
    end

    def chunk_numbers_by_triple_digits
      yield.gsub(/(?<=^|\s)\d{5,}(?=$|\s)/) do |m|
        m.reverse
          .gsub(/...(?=.)/, "\\&#{NBS}")
          .reverse
      end
    end
end
