require 'minitest/autorun'
require_relative '../lib/dashes'

describe Dashes do
  before do
    @hyphen = Dashes::HYPHEN
    @emdash = Dashes::EMDASH
    @endash = Dashes::ENDASH
  end

  it 'replaces hyphen-minus and en-dash after blank '\
    'or the beginning of line or after <p>-tag with em-dash' do

    Dashes.handle { @hyphen }.must_equal @emdash
    Dashes.handle { @endash }.must_equal @emdash
    Dashes.handle { @emdash }.must_equal @emdash

    Dashes.handle { " #@hyphen" }.must_equal " #@emdash"
    Dashes.handle { " #@endash" }.must_equal " #@emdash"
    Dashes.handle { " #@emdash" }.must_equal " #@emdash"
  end

  it 'replaces en-dash and em-dash with hyphen-minus '\
    'just after a word' do

    Dashes.handle { "кое#@endash" }.must_equal "кое#@hyphen"
    Dashes.handle { "кое#@emdash" }.must_equal "кое#@hyphen"
  end

  it 'replaces any dash between two digits (including roman)  with en-dash '\
    'and respects spaces around dash (if any)' do

    Dashes.handle { "4 #@hyphen 5" }.must_equal "4 #@endash 5"
    Dashes.handle { "4 #@endash 5" }.must_equal "4 #@endash 5"
    Dashes.handle { "4 #@emdash 5" }.must_equal "4 #@endash 5"

    Dashes.handle { "4 #{@hyphen} 5" }.must_equal "4 #{@endash} 5"
    Dashes.handle { "4 #{@endash} 5" }.must_equal "4 #{@endash} 5"
    Dashes.handle { "4 #{@emdash} 5" }.must_equal "4 #{@endash} 5"

    Dashes.handle { "4#{@hyphen}5" }.must_equal "4#{@endash}5"
    Dashes.handle { "4#{@endash}5" }.must_equal "4#{@endash}5"
    Dashes.handle { "4#{@emdash}5" }.must_equal "4#{@endash}5"
  end

  it 'converts double hyphen-minus into emdash as for a single hyphen do' do
    Dashes.handle { " #{@hyphen * 2} "  }.must_equal " #@emdash "
    Dashes.handle { @hyphen * 2         }.must_equal @emdash
  end

  it 'keeps endash just before a digit' do
    example = '–43'
    Dashes.handle {  example }.must_equal example
  end
end
