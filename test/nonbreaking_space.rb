require 'minitest/autorun'
require_relative '../lib/nonbreaking_space'

describe NonbreakingSpace do
  before do
    @nbs = NonbreakingSpace::NBS
    @emdash = Dashes::EMDASH
  end

  it 'swap space between a punctuation and emdash with non-breaking space' do
    source = "два, #@emdash"
    expect = "два,#@nbs#@emdash"
    NonbreakingSpace.handle { source }.must_equal expect
  end

  it 'replaces a space between roman digit and `в.`' do
    NonbreakingSpace.handle { 'XX в.' }.must_equal "XX#{@nbs}в."
  end

  it 'replaces a space between a digit and `г.`' do
    NonbreakingSpace.handle { '1 грамм' }.must_equal "1 грамм"
    NonbreakingSpace.handle { '1 г.'    }.must_equal "1#{@nbs}г."
  end

  it 'replaces a space between a digit and `гг.`' do
    NonbreakingSpace.handle { '2000 годы' }.must_equal "2000 годы"
    NonbreakingSpace.handle { '2000 гг.'  }.must_equal "2000#{@nbs}гг."
  end

  it 'replaces a space between a digit and `тыс.`' do
    NonbreakingSpace.handle { '10 тысяч' }.must_equal "10 тысяч"
    NonbreakingSpace.handle { '10 тыс.'  }.must_equal "10#{@nbs}тыс."
  end

  it 'replaces a space between a digit and `%`' do
    NonbreakingSpace.handle { '100 %' }.must_equal "100#{@nbs}%"
  end

  it 'replaces a space between a digit and `км`' do
    NonbreakingSpace.handle { '100 кмп'  }.must_equal "100 кмп"
    NonbreakingSpace.handle { '100 км/ч' }.must_equal "100#{@nbs}км/ч"
  end

  it 'replaces a space between a digit and `кг`' do
    NonbreakingSpace.handle { '100 кгс'  }.must_equal "100 кгс"
    NonbreakingSpace.handle { '100 кг'   }.must_equal "100#{@nbs}кг"
  end

  it 'replaces a space between a digit and `млн`' do
    NonbreakingSpace.handle { '100 млн' }.must_equal "100#{@nbs}млн"
  end

  it 'replaces a space between a digit and `млрд`' do
    NonbreakingSpace.handle { '100 млрд' }.must_equal "100#{@nbs}млрд"
  end

  it 'replaces a space between a digit and `м`' do
    NonbreakingSpace.handle { '10 метров' }.must_equal "10 метров"
    NonbreakingSpace.handle { '10 м'      }.must_equal "10#{@nbs}м"
  end

  it 'replaces a space between a digit and `трлн`' do
    NonbreakingSpace.handle { '100 трлн' }.must_equal "100#{@nbs}трлн"
  end

  it 'replaces a space between a digit and `га`' do
    NonbreakingSpace.handle { '10 гаек' }.must_equal "10 гаек"
    NonbreakingSpace.handle { '10 га'   }.must_equal "10#{@nbs}га"
  end

  it 'replaces a space or inserts nbs between `№` or `§` and a digit' do
    NonbreakingSpace.handle { '№ 6'     }.must_equal "№#{@nbs}6"
    NonbreakingSpace.handle { '№ шесть' }.must_equal "№ шесть"
    NonbreakingSpace.handle { '№6'      }.must_equal "№#{@nbs}6"
    NonbreakingSpace.handle { '№шесть'  }.must_equal "№шесть"
  end

  it 'replaces a space between a digit and `гл.`' do
    NonbreakingSpace.handle { 'глава 15' }.must_equal "глава 15"
    NonbreakingSpace.handle { 'гл. 15'   }.must_equal "гл.#{@nbs}15"
    NonbreakingSpace.handle { 'Гл. 15'   }.must_equal "Гл.#{@nbs}15"
  end


  it 'replace space inside certain short phrases' do
    NonbreakingSpace.handle { 'и т.д.'  }.must_equal "и#{@nbs}т.#{@nbs}д."
    NonbreakingSpace.handle { 'и т. д.' }.must_equal "и#{@nbs}т.#{@nbs}д."
    NonbreakingSpace.handle { 'И т.д.'  }.must_equal "И#{@nbs}т.#{@nbs}д."
    NonbreakingSpace.handle { 'И т. д.' }.must_equal "И#{@nbs}т.#{@nbs}д."
    NonbreakingSpace.handle { 'и т. да' }.must_equal "и т. да"
    NonbreakingSpace.handle { 'и ты д.' }.must_equal "и ты д."
    NonbreakingSpace.handle { 'И ты д.' }.must_equal "И ты д."

    NonbreakingSpace.handle { 'а. е.'  }.must_equal "а.#{@nbs}е."
    NonbreakingSpace.handle { 'а.е.'   }.must_equal "а.#{@nbs}е."
    NonbreakingSpace.handle { 'на. е.' }.must_equal "на. е."
  end

  it 'replace spaces between initials' do
    NonbreakingSpace.handle { 'У.Г.Кришнамурти'   }
      .must_equal "У.#{@nbs}Г.#{@nbs}Кришнамурти"

    NonbreakingSpace.handle { 'У. Г. Кришнамурти' }
      .must_equal "У.#{@nbs}Г.#{@nbs}Кришнамурти"
  end

  it 'replace space between a digit and tree digits' do
    NonbreakingSpace.handle { '1 000' }.must_equal "1#{@nbs}000"
    NonbreakingSpace.handle { '1 000 000' }.must_equal "1#{@nbs}000#{@nbs}000"
    NonbreakingSpace.handle { '1 00' }.must_equal "1 00"
    NonbreakingSpace.handle { '1 00 00' }.must_equal "1 00 00"
  end

  it 'chunk numbers with lenght of 5 and more by triple digits' do
    NonbreakingSpace.handle { '12345'     }.must_equal "12#{@nbs }345"
    NonbreakingSpace.handle { '12345 '    }.must_equal "12#{@nbs }345 "
    NonbreakingSpace.handle { ' 12345'    }.must_equal " 12#{@nbs}345"
    NonbreakingSpace.handle { '123456'    }.must_equal "123#{@nbs}456"
    NonbreakingSpace.handle { '1 2 3 4 5' }.must_equal "1 2 3 4 5"
    NonbreakingSpace.handle { '0,000001'  }.must_equal "0,000001"
    NonbreakingSpace.handle { '123456v'   }.must_equal "123456v"
    NonbreakingSpace.handle { 'arXiv:hep-th/0301240v2'  }
      .must_equal "arXiv:hep-th/0301240v2"
  end
end
