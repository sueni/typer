require 'minitest/autorun'
require_relative '../lib/multimarks'

describe Multimarks do
  it 'replaces `!?` with `?!`' do
    Multimarks.handle { 'foo!?' }.must_equal 'foo?!'
  end

  it 'converts only 3 or 4 sequential dots into omission mark' do
    Multimarks.handle { '...'   }.must_equal '…'
    Multimarks.handle { '....'  }.must_equal '…'
    Multimarks.handle { '.....' }.must_equal '.....'
  end

  it 'reorder dots and exclamation-question marks' do
    Multimarks.handle { '?!.'  }.must_equal '?!'
    Multimarks.handle { '?!..' }.must_equal '?!'
  end

  it 'handles all the mixes of above cases' do
    Multimarks.handle { '?..'    }.must_equal '?..'
    Multimarks.handle { '?...'   }.must_equal '?..'
    Multimarks.handle { '?....'  }.must_equal '?..'

    Multimarks.handle { '!..'    }.must_equal '!..'
    Multimarks.handle { '!...'   }.must_equal '!..'
    Multimarks.handle { '!....'  }.must_equal '!..'

    Multimarks.handle { '?!....' }.must_equal '?!'
    Multimarks.handle { 'foo..'  }.must_equal 'foo.'
    Multimarks.handle { 'foo.'   }.must_equal 'foo.'
  end
end
