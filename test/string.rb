require 'minitest/autorun'
require_relative '../lib/string'

describe String do
  before do
    @hyphen = Dashes::HYPHEN
    @emdash = Dashes::EMDASH
    @endash = Dashes::ENDASH
    @nbs    = NonbreakingSpace::NBS
  end

  it 'handles spaces only' do
    '  foo  '.typo.must_equal ' foo '
  end

  it 'handles spaces and multimarks' do
    '  foo?!..'.typo.must_equal ' foo?!'
    '  foo..'.typo.must_equal ' foo.'
  end

  it 'make hyphen into emdash after [.,?!)] and places nbs in between' do
    ',-'.typo.must_equal ",#@nbs#@emdash"
    '?-'.typo.must_equal "?#@nbs#@emdash"
  end
end
