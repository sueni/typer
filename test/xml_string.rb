require 'minitest/autorun'
require_relative '../lib/xml_string'

describe 'chunk on editable' do
  before do
    @tags = %i{annotation body}
  end

  it 'should chunk on `body` tag without attributes' do
    xml = 'foo<body>bar</body>foo'
    XMLString.new(xml).chunk_with_editable(@tags).to_a
      .must_equal %w[foo <body>bar</body> foo]
  end

  it 'should chunk on `body` tag even with attributes' do
    xml = 'foo<body name="notes">bar</body>foo'
    XMLString.new(xml).chunk_with_editable(@tags).to_a
      .must_equal ['foo', '<body name="notes">bar</body>', 'foo']
  end
end

describe 'split on tags' do
  it 'should find all tags' do
    source = '<annotation>foo<a l:href="#n_1" type="note">[1]</a></annotation>'
    expect = [
      '',
      '<annotation>',
      'foo',
      '<a l:href="#n_1" type="note">',
      '[1]',
      '</a>',
      '',
      '</annotation>'
    ]
    XMLString.new(source).split_on_tags.to_a.must_equal expect
  end
end


describe 'shift note after punctuation' do
  it 'should place note after the dot' do
    source = '<p>тест<a l:href="#n_1" type="note">[1]</a>.</p>'
    expect = '<p>тест.<a l:href="#n_1" type="note">[1]</a></p>'
    XMLString.new(source).shift_note_after_punctuation.to_s.must_equal expect
  end

  it 'should shift the note after dot and quote' do
    source = '<p>тест<a l:href="#n_1" type="note">[1]</a>».</p>'
    expect = '<p>тест».<a l:href="#n_1" type="note">[1]</a></p>'
    XMLString.new(source).shift_note_after_punctuation.to_s.must_equal expect
  end

  it 'should not shift the note with curly braces after the dot' do
    source = '<p>тест<a l:href="#n_1" type="note">{1}</a>.</p>'
    XMLString.new(source).shift_note_after_punctuation.to_s.must_equal source
  end
end


describe 'Pull spaces out of tags' do
  it 'should pull trailing space out of :emphasis and remove it' do
    source = '<p><emphasis>Что-то там </emphasis></p>'
    expect = '<p><emphasis>Что-то там</emphasis></p>'
    XMLString.new(source).purify_tag(:emphasis).to_s.must_equal expect
  end

  it 'should pull trailing space out of :emphasis and remove all' do
    source = '<p><emphasis>Что-то там </emphasis> </p>'
    expect = '<p><emphasis>Что-то там</emphasis></p>'
    XMLString.new(source).purify_tag(:emphasis).to_s.must_equal expect
  end

  it 'should pull trailing space out of :strong' do
    source = 'Выслушай <strong>вслед </strong> за книгой'
    expect = 'Выслушай <strong>вслед</strong>  за книгой'
    XMLString.new(source).purify_tag(:strong).to_s.must_equal expect
  end

  it 'should pull trailing space out of :strong and remove it' do
    source = '<p><strong>Что-то там </strong></p>'
    expect = '<p><strong>Что-то там</strong></p>'
    XMLString.new(source).purify_tag(:strong).to_s.must_equal expect
  end

  it 'should pull trailing space out of :strong and :emphasis tags' do
    source = '<p><strong>один <emphasis>два </emphasis> три </strong></p>'
    expect = '<p><strong>один <emphasis>два</emphasis>  три</strong></p>'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end

  it 'should pull trailing space out of nested tags' do
    source = '<p><strong>один <emphasis>два </emphasis> три </strong> </p>'
    expect = '<p><strong>один <emphasis>два</emphasis>  три</strong></p>'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end

  it 'should pull heading and trailing spaces' do
    source = '<p><strong> один <emphasis> два </emphasis> три </strong> </p>'
    expect = '<p><strong>один  <emphasis>два</emphasis>  три</strong></p>'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end

  it 'should preserve heading space from pulling between tags' do
    source = '</a><emphasis> Уотергейт</emphasis>'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal source
  end

  it 'should squeeze spaces into center' do
    source = '<p><strong> один </strong><emphasis> два </emphasis></p>'
    expect = '<p><strong>один </strong> <emphasis>два</emphasis></p>'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end

  it 'should pull spaces into center' do
    source = '<strong>Джед: </strong><emphasis>Твои </emphasis>отвратительны.'
    expect = '<strong>Джед: </strong><emphasis>Твои</emphasis> отвратительны.'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect

    source = '<emphasis>Джед: </emphasis><strong>Твои </strong>отвратительны.'
    expect = '<emphasis>Джед: </emphasis><strong>Твои</strong> отвратительны.'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end

  it 'squeezes spaces to the left' do
    source = '<p><strong> один <emphasis> два </emphasis></strong></p>'
    expect = '<p><strong>один  <emphasis>два</emphasis></strong></p>'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end

  it 'should remove surrounding spaces near <p> boundaries' do
    source = '<p> Простое </p>'
    expect = '<p>Простое</p>'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end
end


describe 'Pull quotes out of tags' do
  it 'should keep both quotes inside tag' do
    source = '<strong>«Foo»</strong> bar baz.'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal source
  end

  it 'should pull single boundary 1st-lvl-quote out of tag' do
    source = 'Foo «bar <strong>baz»</strong>.'
    expect = 'Foo «bar <strong>baz</strong>».'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end

  it 'should pull single boundary 2st-lvl-quote out of tag' do
    source = 'Foo „bar <strong>baz“</strong>.'
    expect = 'Foo „bar <strong>baz</strong>“.'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end

  it 'should keep quotes inside tag unless they are boundary' do
    source = '<emphasis>«Я», «ты», «он» — лишь видимые проявления в сознании</emphasis>'
    expect = '<emphasis>«Я», «ты», «он» — лишь видимые проявления в сознании</emphasis>'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end

  it 'should keep quotes inside tag unless they are boundary' do
    source = '<p>... сочетания <emphasis>«пространство-время»</emphasis> невозможно?</p>'
    expect = '<p>... сочетания <emphasis>«пространство-время»</emphasis> невозможно?</p>'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end

  it 'should keep quotes inside tag unless they are boundary' do
    source = '<emphasis>мы лишь видимые проявления в „сознании“</emphasis>'
    expect = '<emphasis>мы лишь видимые проявления в „сознании“</emphasis>'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end

  it 'should handle corner case' do
    source = '<emphasis>«мы лишь видимые проявления в „сознании“</emphasis>'
    expect = '«<emphasis>мы лишь видимые проявления в „сознании“</emphasis>'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end

  it 'should handle corner case' do
    source = '<emphasis>«Я», «ты», «он» — лишь ... это — Единое «Я»</emphasis>'
    expect = '<emphasis>«Я», «ты», «он» — лишь ... это — Единое «Я»</emphasis>'
    XMLString.new(source).purify_tag(:strong, :emphasis).to_s.must_equal expect
  end
end
