require 'minitest/autorun'
require_relative '../lib/spaces'

describe Spaces do
  before do
    @oqs = Quote::OPENING_QUOTES
    @cqs = Quote::CLOSING_QUOTES
    @dashes = Dashes::DASHES
    @nbs = NonbreakingSpace::NBS
  end

  it 'should keep space after punctuation' do
    Spaces.handle { 'память ,когда' }.must_equal 'память, когда'
    Spaces.handle { 'память  ,когда' }.must_equal 'память, когда'
    Spaces.handle { 'память , когда' }.must_equal 'память, когда'
  end

  it 'squeezes spaces' do
    Spaces.handle { '  foo  '         }.must_equal ' foo '
    Spaces.handle { ' foo  foo  foo ' }.must_equal ' foo foo foo '
  end

  it 'removes spaces before certain punctuals' do
    Spaces::NO_SPACE_BEFORE_PUNCTS.chars.each do |punct|
      Spaces.handle { " #{punct}" }.must_equal "#{punct} "
    end
  end

  it 'removes spaces after opening quote and before the closing one' do
    @oqs.each_with_index do |oq, i|
      source = "#{oq} foo #{@cqs[i]}"
      expect = "#{oq}foo#{@cqs[i]}"
      Spaces.handle { source }.must_equal expect
    end
  end

  it 'removes spaces inside км/ч' do
    Spaces.handle { '100 км / ч'    }.must_equal '100 км/ч'
    Spaces.handle { '100 км/ ч'     }.must_equal '100 км/ч'
    Spaces.handle { '100 км /ч'     }.must_equal '100 км/ч'
    Spaces.handle { '100 км / час'  }.must_equal '100 км / час'
  end

  it 'removes spaces inside м/c' do
    Spaces.handle { '100 м / с'    }.must_equal '100 м/с'
    Spaces.handle { '100 м/ с'     }.must_equal '100 м/с'
    Spaces.handle { '100 м /с'     }.must_equal '100 м/с'
    Spaces.handle { '100 м / сек.' }.must_equal '100 м / сек.'
  end

  it 'handles mix of all the above cases' do
    source = '  « foo ,   bar ;»  '
    expect = ' «foo, bar;» '
    Spaces.handle { source }.must_equal expect
  end

  it 'ensures there is a space between certain puncts and dashes' do
    Spaces::SPACE_AFTER_PUNCTS.chars.each do |punct|
      @dashes.chars.each do |dash|
        Spaces.handle { "#{punct}#{dash}" }.must_equal "#{punct}#@nbs#{dash}"
      end
    end
  end
end
