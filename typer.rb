#!/usr/bin/ruby -w
# frozen_string_literal: true
# ──────────────────────────────────────────────────────────────────────────────
# Takes several files as args or one file from STDIN and handles them
# accordingly
# ──────────────────────────────────────────────────────────────────────────────

%w{
  string
  xml_string
}.each { |m| require_relative File.join('lib', m) }

class Typer
  def initialize(infile)
    unless File.exist?(infile)
      abort "File not exists: #{infile}"
    end
    @infile  = infile
    @outfile = build_outfile_name
  end

  def call
    if is_xml?
      handle_fb2
      final_prettify_fb2
    else
      handle_plain_text
    end
  end

  private

    def is_xml?
      File.open(@infile) { |f| f.getc == ?< }
    end

    def handle_plain_text
      File.open(@outfile, 'w') do |f|
        File.foreach(@infile) { |line| f.puts(line.typo.rstrip) }
      end
    end

    def handle_fb2
      xml = XMLString.new(File.read(@infile))
      xml.prettyfy
      chunks_with_editables = xml.chunk_with_editable(:annotation, :body)

      result = chunks_with_editables.map.with_index do |e_chunk, e_idx|
        if e_chunk_editable?(e_idx)
          XMLString.new(e_chunk).
            # shift_note_after_punctuation. XXX
            purify_tag(:strong, :emphasis).
            split_on_tags.
            map.with_index do |a_tag_chunk, a_idx|
              a_chunk_editable?(a_idx) ? a_tag_chunk.typo : a_tag_chunk
            end.join

        else
          e_chunk
        end
      end.join

      save(result)
    end

    def e_chunk_editable?(idx)
      idx.odd?
    end

    def a_chunk_editable?(idx)
      idx.even?
    end

    def save(result)
      File.write(@outfile, result)
    end

    def final_prettify_fb2
      File.open(@outfile, 'r+') do |f|
        f.write XMLString.new(File.read(@outfile)).prettyfy
      end
    end

    def build_outfile_name
      File.basename(@infile, '.*') + '.t' + File.extname(@infile)
    end
end

if STDIN.tty?
  Typer.new(ARGV.shift).call
else
  ARGF.each_line do |line|
    puts line.typo.rstrip
  end
end
